let arrayFiles = []
let arrayFilesUp = []
converFiles = function(event){
    let arrayFHTML = ''
    if (event.files) {
        console.log(event.files)
        arrayFiles = [...event.files]
        arrayFiles.forEach((element, index) => {
            arrayFHTML += '<div class="title-load-image d-flex flex-wrap align-items-center justify-content-between">'+
                            '<span>'+element.name+'</span>'+
                            '<i onclick="removeFile(\''+index+'\')" class="fas fa-times"></i>'+
                          '</div>';
        });
        document.getElementById('archivosList').innerHTML = arrayFHTML
    }
}

removeFile = function(event){
    document.getElementById('archivosList').innerHTML = ''
    arrayFiles.splice(event, 1);
    let arrayFHTML = '';
    arrayFiles.forEach((element, index) => {
        fileToBase64(element)
        arrayFHTML += '<div class="title-load-image d-flex flex-wrap align-items-center justify-content-between">'+
                        '<span>'+element.name+'</span>'+
                        '<i onclick="removeFile('+index+')" class="fas fa-times"></i>'+
                      '</div>';
    });
    document.getElementById('archivosList').innerHTML = arrayFHTML;
}


cancelarSubirArchivos = function(){
    document.getElementById('content-cargar-documentos').style.display = 'block'
    document.getElementById('content-subir archivos').style.display = 'none'
    document.getElementById('content-cargar-documentos').style.display = 'block'
    document.getElementById('content-subir archivos').style.display = 'none'
    arrayFiles = []
    document.getElementById('archivosList').innerHTML = ''
    arrayFilesUp = []
}

subirArchivos = function () {
    document.getElementById('content-cargar-documentos').style.display = 'none'
    document.getElementById('content-subir archivos').style.display = 'block'
    setTimeout(() => {
        document.getElementById('content-cargar-documentos').style.display = 'block'
        document.getElementById('content-subir archivos').style.display = 'none'
        arrayFiles = []
        document.getElementById('archivosList').innerHTML = ''
    }, 3600);

    arrayFiles.forEach(file => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            let fileBase64 = reader.result.toString().split(",")[1];
            var nombreFile = file.name.split(".");
            let extension = "";
            if (nombreFile.length > 1 && nombreFile[nombreFile.length - 1] !== "") {
                extension = "." + nombreFile.pop();
            }
            let nombre = nombreFile.join(".");
            arrayFilesUp.push({
                archivo: fileBase64,
                nombre: nombre,
                extension: extension
            });
        }
    })

    console.log(arrayFilesUp)
}

