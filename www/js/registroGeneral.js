var idioma_espanol_not_font = {
	"sProcessing": "Procesando...",
	"sLengthMenu": "Mostrar _MENU_ registros",
	"sZeroRecords": "No se encontrar\u00F3n resultados",
	"sEmptyTable": "Ning\u00Fan dato disponible en esta tabla",
	"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
	"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
	"sInfoPostFix": "",
	"sSearch": "Buscar:",
	"sUrl": "",
	"sInfoThousands": ",",
	"sLoadingRecords": "Cargando...",
	"oPaginate": {
		"sFirst": "Primero",
		"sLast": "Último",
		"sNext": "<i class='fa fa-chevron-right fa-3' aria-hidden='true'></i>",
		"sPrevious": "<i class='fa fa-chevron-left fa-3' aria-hidden='true'></i>"
	},
	"oAria": {
		"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
};
$(document).ready(function() {
    var table = $('#datatable-registro-general').DataTable({
        "order": [],
		"ordering": false,
		"searching": false,
		"lengthChange": false,
        "language": idioma_espanol_not_font
    });

	var tableDocumento = $('#datatable-registro-documento').DataTable({
        "order": [],
		"ordering": false,
		"searching": false,
		"lengthChange": false,
        "language": idioma_espanol_not_font
    });

    
    $('#mySelect').on('change', function () {
        var selectedValue = $(this).val(); // Obtiene el valor seleccionado
        table.page.len(selectedValue).draw(); // Cambia la longitud de página y redibuja la tabla
    });
});